**MongoDB University Courses**  

*Data Wrangling*  
* [1-Parsing HTML](https://github.com/joelzou925/MongoDB/blob/master/DataWrangling/1-ParsingHTML.ipynb)
* [2-Auditing Data Quality](https://github.com/joelzou925/MongoDB/blob/master/DataWrangling/2-CheckDataQuality.ipynb)
* [3-Load Data into MongoDB](https://github.com/joelzou925/MongoDB/blob/master/DataWrangling/3-LoadDB.ipynb)
* [4-Aggregation Framework](https://github.com/joelzou925/MongoDB/blob/master/DataWrangling/4-AggregationQuiz.ipynb)
* [5-OpenStreetMap Case Study](https://github.com/joelzou925/MongoDB/blob/master/DataWrangling/5-OpenStreetMap.ipynb) 

*Data Analytics Part 1*
* [1-Connecting to Atlas](https://github.com/joelzou925/MongoDB/blob/master/Analytics1/connecting-to-atlas.ipynb)
* [2-Analyzing Data with aggregation](https://github.com/joelzou925/MongoDB/blob/master/Analytics1/analyzing-data-with-aggregation.ipynb)
* [3-Cleaning Data with updates](https://github.com/joelzou925/MongoDB/blob/master/Analytics1/cleansing-data-with-updates.ipynb)
* [4-Querying for documents on an array field](https://github.com/joelzou925/MongoDB/blob/master/Analytics1/querying-for-documents-on-an-array-field.ipynb)
* [5-Improving Query performance](https://github.com/joelzou925/MongoDB/blob/master/Analytics1/improve-query-performance.ipynb)
* [6-Finding things nearby](https://github.com/joelzou925/MongoDB/blob/master/Analytics1/finding-things-nearby.ipynb)
* [7-Mapping Geodata](https://github.com/joelzou925/MongoDB/blob/master/Analytics1/mapping-geodata.ipynb)
* [8-Creating Cartesian plots](https://github.com/joelzou925/MongoDB/blob/master/Analytics1/creating-cartesian-plots.ipynb)
* [9-Creating 3D plots](https://github.com/joelzou925/MongoDB/blob/master/Analytics1/creating-3d-plots.ipynb)
* [10-Creating Boxplots](https://github.com/joelzou925/MongoDB/blob/master/Analytics1/creating-box-plots.ipynb)

*Data Analytics Part 2*
* [1-Expression with Project](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/expressions_with_project.ipynb)
* [2-Changing document shape](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/changing_document_shape.ipynb)
* [3-Aggregation find Favorite Movies](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/cursor_like_methods.ipynb)
* [4-Group Accumulator Statistics](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/lab_group_accumulators.ipynb)
* [5-Unwind and Group](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/unwind_and_group_lab.ipynb)
* [6-Lookup joins](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/lookup_lab.ipynb)
* [7-GraphLookup Degree of Separation](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/lab__graphlookup.ipynb)
* [8-Schema and Accumulator](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/schemas-and-accumulators.ipynb)
* [9-Lookup and Entity Resolution](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/schemas-and-accumulators.ipynb)
* [10-Pearson Correlation](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/pearson_correlation__lab.ipynb)
* [11-Linear Regression](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/linear-regression-on-titanic-data-set.ipynb)
* [12-Decision Tree](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/Decision%2BTree.ipynb)
* [13-Kmeans Clustering](https://github.com/joelzou925/MongoDB/blob/master/Analytics2/kmeans.ipynb)

*M121 Aggregation Framework*
* [1-Match and Project](https://github.com/joelzou925/MongoDB/blob/master/Aggregation/Chapter1.ipynb)
* [2-Utility Stages](https://github.com/joelzou925/MongoDB/blob/master/Aggregation/Chapter2.ipynb)
* [3-Group and Lookup](https://github.com/joelzou925/MongoDB/blob/master/Aggregation/Chapter3.ipynb)
* [4-Multidimensional Grouping](https://github.com/joelzou925/MongoDB/blob/master/Aggregation/Chapter4.ipynb) 